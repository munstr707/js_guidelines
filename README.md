# JavaScript Guidelines

## Why?

Javascript has grown into one of the most popular programming languages of all time yet it is also frequently misunderstood and is evolving quickly.

In 2015 ES6 released many years after ES5, the previous version of JavaScript, and introduced an entire books worth of new features. ES5 was also a large update to the language. Both of these releases unveiled more than a books worth of new features, and since ES6 JavaScript has switched to a yearly release cycle for updating the language.

This guide is an opinionated way to navigate all the changes to the JavaScript language and to write great code in the langauge. JavaScript is not going anywhere, and its likely that our future only includes more of it. Lets do the best we can!

## Table of Contents

1.  Introduction
2.  Core Language
3.  Functional Programming
4.  Features
    1.  [const > let > var](./4.%20Language%20Features/1.%20const%20>%20let%20>%20var.md)
    2.  [Object.freeze()](<./4.%20Language%20Features/2.%20Object.freeze().md>)
    3.  [Arrow Functions](./4.%20Language%20Features/3.%20Arrow%20Functions.md)
    4.  [Array Higher Order Functions](./4.%20Language%20Features/4.%20Array%20Higher%20Order%20Functions.md)
5.  Code Organization

<!-- Organization in two sections. 1. Content in order 2. An index of the content that breaks it down -->

## Upcoming Content!

- Core Language
  - The Prototype
  - Event loop
  - Typing
- ES5, ES6 and beyond features
  - Array Higher Order Functions
  - Destructuring
  - Named Arguments
  - Enums
  - Rest/Spread
- Functional Programming
  - Higher Order Functions
  - First Class Functions
  - Pure Functions
  - Immutability
  - Functors
- Code Organization
  - Closures
  - Classes
  - Closures vs Classes
  - Closure Proposal
  - Right tool for the right job
  - ES6 Modules
  - IIFE
- Technologies
  - JQuery or not to JQuery
